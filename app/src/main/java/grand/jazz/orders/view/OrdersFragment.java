package grand.jazz.orders.view;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gjiazhe.panoramaimageview.GyroscopeObserver;

import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.databinding.FragmentOrdersBinding;
import grand.jazz.orders.request.OrderRequest;
import grand.jazz.orders.viewmodel.OrdersViewModel;

public class OrdersFragment extends BaseFragment {
     View rootView;
    OrdersViewModel ordersViewModel;
    FragmentOrdersBinding fragmentOrdersBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOrdersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentOrdersBinding.getRoot();
        ordersViewModel = new OrdersViewModel(new OrderRequest(1));
        fragmentOrdersBinding.setOrdersViewModel(ordersViewModel);
    }


    private void liveDataListeners() {
        ordersViewModel.getClicksMutableLiveData().observe(this, result -> {


        });
    }




}