
package grand.jazz.orders.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import grand.jazz.R;
import grand.jazz.databinding.ItemOrderBinding;
import grand.jazz.orders.response.OrderItem;
import grand.jazz.orders.viewmodel.OrderItemViewModel;


public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.CategoriesViewHolder> {

    private List<OrderItem> orderItems;


    public OrdersAdapter() {
        this.orderItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        OrderItem dataModel = orderItems.get(position);
        OrderItemViewModel orderItemViewModel = new OrderItemViewModel(dataModel,position);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.orderItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<OrderItem> data) {

            this.orderItems.clear();

            this.orderItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemOrderBinding itemOrderBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemOrderBinding == null) {
                itemOrderBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemOrderBinding != null) {
                itemOrderBinding.unbind();
            }
        }

        void setViewModel(OrderItemViewModel serviceItemViewModel) {
            if (itemOrderBinding != null) {
                itemOrderBinding.setOrderItemViewModel(serviceItemViewModel);
            }
        }


    }



}
