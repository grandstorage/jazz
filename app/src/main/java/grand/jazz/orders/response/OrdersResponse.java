package grand.jazz.orders.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrdersResponse {

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<OrderItem> data;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<OrderItem> data){
		this.data = data;
	}

	public List<OrderItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ServicesResponse{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}