package grand.jazz.orders.response;


import com.google.gson.annotations.SerializedName;


public class OrderItem{

	@SerializedName("date")
	private String date;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("worker")
	private Worker worker;

	@SerializedName("status")
	private String status;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setWorker(Worker worker){
		this.worker = worker;
	}

	public Worker getWorker(){
		return worker;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderItem{" + 
			"date = '" + date + '\'' + 
			",id = '" + id + '\'' + 
			",time = '" + time + '\'' + 
			",worker = '" + worker + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}