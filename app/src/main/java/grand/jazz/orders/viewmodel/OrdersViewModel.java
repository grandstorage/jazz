package grand.jazz.orders.viewmodel;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.orders.request.OrderRequest;
import grand.jazz.orders.response.OrdersResponse;
import grand.jazz.orders.view.OrdersAdapter;

public class OrdersViewModel extends BaseViewModel {

    OrdersAdapter ordersAdapter;

    public OrdersViewModel(OrderRequest orderRequest) {
        getServices(orderRequest);
    }


    @BindingAdapter({"app:adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, OrdersAdapter categoriesAdapter) {
        recyclerView.setAdapter(categoriesAdapter);
    }


    @Bindable
    public OrdersAdapter getOrdersAdapter() {
        return this.ordersAdapter == null ? this.ordersAdapter = new OrdersAdapter() : this.ordersAdapter;
    }


    private void getServices(OrderRequest orderRequest) {

        OrderRequest orderRequest1 = new OrderRequest(1);

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                OrdersResponse servicesResponse = (OrdersResponse)response;
                getOrdersAdapter().updateData(servicesResponse.getData());
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.ORDERS_HISTORY, orderRequest, OrdersResponse.class);
    }
}
