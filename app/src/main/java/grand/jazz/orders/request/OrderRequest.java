package grand.jazz.orders.request;

import com.google.gson.annotations.SerializedName;

public class OrderRequest  {

    @SerializedName("user_id")
    int userId;

    public OrderRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
