package grand.jazz;

import android.Manifest;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import grand.jazz.base.MovementManager;
import grand.jazz.base.ParentActivity;
import grand.jazz.base.views.SplashScreenViewModel;
import grand.jazz.chatmessages.view.ChatMessagesFragment;
import grand.jazz.createorder.request.CreateOrderRequest;
import grand.jazz.createorder.view.CreateOrderFragment;
import grand.jazz.databinding.ActivityBaseBinding;
import grand.jazz.home.view.HomeFragment;
import grand.jazz.orders.view.OrdersFragment;
import grand.jazz.services.view.ServicesFragment;


public class MainActivity extends ParentActivity {
    public ActivityBaseBinding activityBaseBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                1);
        MovementManager.addFragment(this, new ChatMessagesFragment(), "");
    }






}
