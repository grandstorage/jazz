package grand.jazz.base;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.PopupMenu;

import java.util.ArrayList;

import grand.jazz.R;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.Params;

public class PopUpMenus {

    public static PopupMenu showPopUp(Context context, View view, ArrayList<PopUpItem> types) {
        PopupMenu typesPopUps;
        typesPopUps = new PopupMenu(context, view);
        for (int i = 0; i < types.size(); i++) {
            typesPopUps.getMenu().add(i, i, i, types.get(i).name);
        }
        typesPopUps.show();

        return typesPopUps;

    }

    public static ArrayList<PopUpItem>  getUserTypes(Context context ){
        ArrayList<PopUpItem> types = new ArrayList<>();
        types.add(new PopUpItem(Codes.TYPE_USER,context.getResources().getStringArray(R.array.userTypes)[0]));
        types.add(new PopUpItem(Codes.TYPE_COMPANY,context.getResources().getStringArray(R.array.userTypes)[1]));
        return types;
    }
}
