package grand.jazz.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by mohamedatef on 1/12/19.
 */

public class MessagesManager {

    public static void fastDialog(final Activity activity, String title, String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int id) {
            }
        });
        builder.create().show();
    }


    public static void lToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void sToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void dToast(Context context, String text, int duration) {

        Toast.makeText(context, text, duration).show();

    }
}
