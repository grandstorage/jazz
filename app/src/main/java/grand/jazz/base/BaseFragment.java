package grand.jazz.base;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.google.gson.annotations.Expose;

import grand.jazz.MainActivity;
import grand.jazz.base.views.BaseActivity;

public class BaseFragment extends Fragment {
    Context context;



    public void accessLoadingBar(int visiablity) {
        try {
            ((BaseActivity) context).activityBaseBinding.pbBaseLoadingBar.setVisibility(visiablity);
        }catch (ClassCastException e){
            e.getStackTrace();
        }
        try {
            ((MainActivity) context).activityBaseBinding.pbBaseLoadingBar.setVisibility(visiablity);
        }catch (ClassCastException e){
            e.getStackTrace();
        }
    }

    public void showMessage(String message) {
        //beacuse of handling snack bar
        ((BaseActivity) context).showMessage(message);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.context = null;
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.context = context;
    }




}
