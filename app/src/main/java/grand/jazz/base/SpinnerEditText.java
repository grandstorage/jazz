package grand.jazz.base;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupMenu;
import java.util.ArrayList;

public class SpinnerEditText extends AppCompatEditText{
    PopupMenu popupMenu;
      ArrayList<PopUpItem> popUpItems ;
    public SpinnerEditText(Context context) {
        super(context);

    }

    public SpinnerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public SpinnerEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


    private void showPopUp(final ArrayList<PopUpItem> popUpItems){
        this.popUpItems=popUpItems;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = PopUpMenus.showPopUp(getContext(), SpinnerEditText.this, popUpItems);
            }
        });

    }


    public PopupMenu getPopUpMenu(){
       return popupMenu;
    }

    public ArrayList<PopUpItem> getPopUpItems() {
        return popUpItems;
    }
}
