package grand.jazz.base.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;

import grand.jazz.base.Validate;

public class EmailEditText extends AppCompatEditText{
    public EmailEditText(Context context) {
        super(context);
        init();
    }

    public EmailEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EmailEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init(){
        setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        addTextChangedListener(new TextWatcher() {

            // the user's changes are saved here
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                if(Validate.isEmpty(c.toString())){
                    setError("Empty");
                }else if(!Validate.isMail(c.toString())){
                    setError("Wrong email");
                }else {
                    setError(null);
                }
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // this space intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // this one too
            }
        });
    }


}
