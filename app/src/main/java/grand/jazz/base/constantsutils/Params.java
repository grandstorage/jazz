package grand.jazz.base.constantsutils;

public class Params {
    //SharedPreferenceParams
    public static final String PREF_USER_NAME="user_name";

    //BundleParams
    public static final String BUNDLE_USER_DETAILS="user_details";

    //IntentParams
    public static final String INTENT_SETTINGS="settings";
    public static final String INTENT_PAGE="page";



}
