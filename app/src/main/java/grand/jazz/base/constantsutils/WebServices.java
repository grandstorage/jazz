package grand.jazz.base.constantsutils;

/**
 * Created by sameh on 4/2/18.
 */

public class WebServices {
    //Status
    public static String REGISTERED ="registered_successfully";
    public static final String SUCCESS = "success";
    public static final String NOT_ACTIVE = "not_active";
    public static final String FAILED="failed";


    public static final String TYPE_SCHEDULED="scheduled";
    public static final String TYPE_URGENT="urgent";
    public static final String TYPE_USER="user";
    public static final String TYPE_WORKER="worker";

    //URLS
    public static String BASE_URL ="http://www.google.com/";
    public static String EMPLOYEE ="employee";
    public static final String LOGIN = "login";
    public static final String SEND_CODE = "code_send";
    public static final String REGISTER = "register";


    //params
    public static String USER_NAME ="user_name";
    public static final String PHONE = "phone";
    public static final String GOOGLE_TOKEN = "google_token";
    public static final String USER_PROFILE = "profile_image";

    public static final String COMMERICAL_REGISTER = "commercial_register";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";



}
