package grand.jazz.base.constantsutils;

public class Codes {

    //RequestsCodes
    public static int FILE_TYPE_IMAGE = 10;
    public static int FILE_TYPE_VIDEO = 11;
    public static int FILE_TYPE_PDF = 12;



    //ACTIONS
    public static int SHOW_LOADING_BAR=20;
    public static int HIDE_LOADING_BAR=21;
    public static int SHOW_MESSAGE=22;
    public static int SAVE_TO_SHARED_PREFERENCE=23;
    public static int SHOW_COUNTRY_MENU=24;
    public static int REGISTER_SCREEN=25;
    public static int FORGOT_PASSWORD_SCREEN=26;
    public static int ENTER_CODE_SCREEN=27;
    public static int CHANGE_PASSWORD_SCREEN=28;
    public static int SEND_CODE_SCREEN=29;
    public static int HOME_SCREEN=30;
    public static int LOGIN_SCREEN=31;



    public static int SELECT_COMMERICAL_IMAGE=229;
    public static int SELECT_PROFILE_IMAGE=210;
    public static int SELECT_ACCOUNT_TYPE=211;
    public static int SELECT_ORDER_IMAGE=212;

    //userTypes
    public static String TYPE_USER="user";
    public static String TYPE_COMPANY="company";

}
