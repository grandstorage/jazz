package grand.jazz.base.volleyutils;

/**
 * Created by mohamedatef on 12/29/18.
 */

public class URLS {

    public static String DEFAULT_LINK = "http://jaz.my-staff.net/api/";
    public static String DEFAULT_LINK_USER = DEFAULT_LINK+"user/";

    public static String FILES_LINK = "http://shoglana.com/public/";
    public static final String LOGIN = DEFAULT_LINK_USER+"login";

    public static final String REGISTER = DEFAULT_LINK_USER+"register";
    public static final String SEND_CODE = DEFAULT_LINK_USER+"code_send";
    public static final String CHECK_CODE=DEFAULT_LINK_USER+"code_check";
    public static final String HOME = DEFAULT_LINK_USER+"category";
    public static final String SERVICES = DEFAULT_LINK_USER+"sub_category";
    public static final String ORDERS_HISTORY = DEFAULT_LINK_USER+"order_history";
    public static final String CREATE_ORDER = DEFAULT_LINK_USER+"order";
    public static final String CHAT_MESSAGES =  DEFAULT_LINK_USER+"get_message";
    public static final String SEND_MESSAGE =  DEFAULT_LINK_USER+"message";

}
