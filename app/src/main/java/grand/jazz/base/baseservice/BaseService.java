package grand.jazz.base.baseservice;

import android.arch.lifecycle.MutableLiveData;

public class BaseService {
    MutableLiveData<Integer> mutableLiveData;

    public BaseService(){
        mutableLiveData= new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getMutableLiveData() {
        return mutableLiveData;
    }
}

