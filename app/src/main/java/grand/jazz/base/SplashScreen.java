package grand.jazz.base;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import grand.jazz.R;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.views.SplashScreenViewModel;
import grand.jazz.chatmessages.view.ChatMessagesFragment;

import grand.jazz.databinding.ActivitySplashScreenBinding;

public class SplashScreen extends ParentActivity {
    ActivitySplashScreenBinding activitySplashScreenBinding;
    SplashScreenViewModel splashScreenViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        splashScreenViewModel = new SplashScreenViewModel();
        liveDataListeners();

    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
    }

    private void liveDataListeners() {
        splashScreenViewModel.getClicksMutableLiveData().observe(this, integer -> MovementManager.startMainActivity(SplashScreen.this, integer));
    }


}
