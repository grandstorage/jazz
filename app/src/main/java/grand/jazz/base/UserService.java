package grand.jazz.base;


import com.android.volley.Request;
import grand.jazz.base.baseservice.BaseService;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.login.response.UserResponse;

public class UserService extends BaseService {


    private void login(UserRequest userRequest) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
        }).requestJsonObject(Request.Method.POST, URLS.DEFAULT_LINK, userRequest, UserResponse.class);
    }




}
