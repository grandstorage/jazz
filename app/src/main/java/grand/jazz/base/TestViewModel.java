package grand.jazz.base;

import android.databinding.BaseObservable;
import android.util.Log;

public class TestViewModel extends BaseObservable{
    DataObject dataObject;
    String name;

    public TestViewModel (){
        dataObject = new DataObject();
    }

    public void performClickAction(){
        Log.e("LOG",""+name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
