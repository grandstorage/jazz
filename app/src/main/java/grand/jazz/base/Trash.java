package grand.jazz.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import grand.jazz.R;
import grand.jazz.base.constantsutils.Codes;


public class Trash {

    /*
      final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_attach_image);
            try {
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
            } catch (Exception e) {
                e.getStackTrace();
            }
            final PhotoView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
            Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
            Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);


            attachImage.setImageBitmap(compressObject.getImage());
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();
     */

    public static void pickImage(final Context context) {
        String choiceString[] = new String[]{"Gallery", "Camera"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Select image from");
        dialog.setItems(choiceString,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent;
                        if (which == 0) {
                            intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        } else {
                            intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                        ((AppCompatActivity)context).startActivityForResult(Intent.createChooser(intent, "Select profile picture"), Codes.FILE_TYPE_IMAGE);
                    }
                }).show();

    }
}
