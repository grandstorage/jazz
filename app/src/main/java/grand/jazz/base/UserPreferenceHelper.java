package grand.jazz.base;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import grand.jazz.login.response.UserItem;

public class UserPreferenceHelper {

    private static SharedPreferences sharedPreferences = null;

    //here you can find shared preference operations like get saved data for user


    private UserPreferenceHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = context.getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }

    public static int getUserId(Context context) {
        return getSharedPreferenceInstance(context).getInt("userId", -1);
    }

    public static void setUserId(Context context, int userId) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putInt("userId", userId);
        prefsEditor.commit();
    }


    public static void setRememberMe(Context context, boolean rememberMe) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("rememberMe", rememberMe);
        prefsEditor.commit();
    }

    public static boolean isRemember(Context context) {
        return getSharedPreferenceInstance(context).getBoolean("rememberMe", false);
    }

    public static void saveUserDetails(Context context, UserItem userModel) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        prefsEditor.putInt("userId", userModel.getId());
        prefsEditor.commit();
    }

    public static void clearUserDetails(Context context) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putString("userDetails", null);
        prefsEditor.putInt("userId", -1);
        prefsEditor.commit();
    }

    public static UserItem getUserDetails(Context context) {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(context).getString("userDetails", "");
        if(json==null||json.equals(""))return new UserItem();
        return gson.fromJson(json, UserItem.class);
    }


    public static boolean isLogined(Context context) {
        boolean isLogined = (getUserDetails(context) != null);
        if (!isLogined);
        // MUT.lToast(context, context.getResources().getString(R.string.please_login));
        return isLogined;
    }


    public static void setLanguage(Context context, String language) {
        SharedPreferences userDetails = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("language", language);
        editor.putBoolean("haveLanguage", true);
        editor.commit();
    }

    public static String getCurrentLanguage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean("haveLanguage", false)) return "en";
        return sharedPreferences.getString("language", "en");
    }

    public static boolean isFirstTime(Context context) {
        boolean isFirstTime = getSharedPreferenceInstance(context).getBoolean("firstTime", true);
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("firstTime", false);
        prefsEditor.commit();

        return isFirstTime;
    }


    public static void saveGoogleToken(Context context, String token) {
        if (token == null || token.equals("")) {
            return;
        } else {
            SharedPreferences.Editor editor = getSharedPreferenceInstance(context).edit();
            editor.putString("googleToken", token);
            editor.commit();
        }
    }

    public static String getGoogleToken(Context context) {
        String token =  getSharedPreferenceInstance(context).getString("googleToken", "")+"";
        return (token.length()<1||token==null)?"EmptyToken":token;
    }


}
