package grand.jazz.base;

import com.google.gson.annotations.Expose;

public class UserRequest {

    String password;
    String phone;
    String token="ddd";
    @Expose
    String error;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }


    public boolean isValid() {
        return (getError().equals("null")||getError()==null)&&password!=null&&phone!=null;
    }


}
