package grand.jazz.base;

public class BaseModel {
   public String error;

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
