package grand.jazz.base.views;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.MovementManager;
import grand.jazz.base.SplashScreen;
import grand.jazz.base.UserPreferenceHelper;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.volleyutils.MyApplication;

public class SplashScreenViewModel extends BaseViewModel {

    public SplashScreenViewModel(){
            startApp();
    }

    @SuppressLint("StaticFieldLeak")
    private void startApp(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {

                if(UserPreferenceHelper.isLogined(MyApplication.getInstance().getApplicationContext())) {
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                }else {
                    getClicksMutableLiveData().setValue(Codes.LOGIN_SCREEN);

                }
            }

        }.execute();
    }
}
