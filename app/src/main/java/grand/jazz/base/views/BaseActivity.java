package grand.jazz.base.views;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.Toast;

import grand.jazz.base.MovementManager;
import grand.jazz.base.ParentActivity;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.Params;
import grand.jazz.databinding.ActivityBaseBinding;;
import grand.jazz.R;
import grand.jazz.home.view.HomeFragment;
import grand.jazz.login.view.LoginFragment;
import grand.jazz.register.view.RegisterFragment;
import grand.jazz.sendcode.view.SendCodeFragment;

public class BaseActivity extends ParentActivity {
    public ActivityBaseBinding activityBaseBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);

        if(getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE,0));
        }
    }



    private void addFragment(int page){

        if(page==Codes.REGISTER_SCREEN) {
            MovementManager.addFragment(this, new RegisterFragment(), "");
        }else   if(page==Codes.SEND_CODE_SCREEN) {
            MovementManager.addFragment(this, new SendCodeFragment(), "");
        }else if(page==Codes.HOME_SCREEN) {
            MovementManager.addFragment(this, new HomeFragment(), "");
        }
        else {
            MovementManager.addFragment(this, new LoginFragment(), "");
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }*/

}
