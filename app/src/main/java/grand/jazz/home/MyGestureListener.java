package grand.jazz.home;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

    private static final float MAX_ZOOM = 0.5f;
    ImageView imageView;
    private static final float PCT = 300f;
    private float delta;

    private ValueAnimator valueAnimator;

    @Override
    public boolean onDown(MotionEvent event) {
        return true;
    }

    public MyGestureListener(ImageView imageView){
        this.imageView=imageView;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }

        delta += distanceY;
        float pct = getPct(delta);
        imageView.setScaleX(1.0f + pct);
        imageView.setScaleY(1.0f + pct);
        return false;
    }

    public void upDetected() {

        float pct = getPct(delta);

        valueAnimator = new ValueAnimator();
        valueAnimator.setFloatValues(pct, 0.0f);
        valueAnimator.addUpdateListener(animation -> {
            imageView.setScaleX(1.0f + (float) animation.getAnimatedValue());
            imageView.setScaleY(1.0f + (float) animation.getAnimatedValue());
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                delta = 0f;
                imageView.setScaleX(1.0f);
                imageView.setScaleY(1.0f);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();
    }

    private float getPct(float delta) {
        float pct = delta / PCT;
        if (pct >= MAX_ZOOM) {
            pct = MAX_ZOOM;
        }
        else if (pct <= -MAX_ZOOM) {
            pct = -MAX_ZOOM;
        }
        return pct;
    }
}