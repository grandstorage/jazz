/*
 * Copyright (c) 2018 Phunware Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package grand.jazz.home.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import java.util.List;

import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.home.response.CategoryItem;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CategoryItemViewModel extends BaseObservable {
    private CategoryItem categoryItem;
    private MutableLiveData<Void> itemsOperationsLiveListener;

    public CategoryItemViewModel(CategoryItem categoryItem, int position) {
        this.categoryItem = categoryItem;
        this.itemsOperationsLiveListener = new MutableLiveData<>();
    }


    public MutableLiveData<Void> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }


    @Bindable
    public CategoryItem getCategoryItem() {
        return categoryItem;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imagePath){
        Log.e("imagePath",imagePath);
        ConnectionHelper.loadImage(view,  imagePath);
    }




}
