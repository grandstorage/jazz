package grand.jazz.home.viewmodel;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.home.response.CategoriesResponse;
import grand.jazz.home.view.CategoriesAdapter;
import grand.jazz.orders.request.OrderRequest;

public class HomeViewModel extends BaseViewModel {

    CategoriesAdapter categoriesAdapter;

    public HomeViewModel() {
        getCategories();
     }


    @BindingAdapter({"app:adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, CategoriesAdapter categoriesAdapter) {
        recyclerView.setAdapter(categoriesAdapter);

    }


    @Bindable
    public CategoriesAdapter getCategoriesAdapter() {
        return this.categoriesAdapter == null ? this.categoriesAdapter = new CategoriesAdapter() : this.categoriesAdapter;
    }


    private void getCategories() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                CategoriesResponse categoriesResponse = (CategoriesResponse) response;
                getCategoriesAdapter().updateData(categoriesResponse.getData().getCategory());
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.HOME, new Object(), CategoriesResponse.class);
    }


}
