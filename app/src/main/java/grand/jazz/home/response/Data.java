package grand.jazz.home.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Data{

	@SerializedName("order_count")
	private int orderCount;

	@SerializedName("category")
	private List<CategoryItem> category;

	public void setOrderCount(int orderCount){
		this.orderCount = orderCount;
	}

	public int getOrderCount(){
		return orderCount;
	}

	public void setCategory(List<CategoryItem> category){
		this.category = category;
	}

	public List<CategoryItem> getCategory(){
		return category;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"order_count = '" + orderCount + '\'' + 
			",category = '" + category + '\'' + 
			"}";
		}
}