package grand.jazz.home.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CategoryItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@Expose
	private String image="https://www.dewetron.com/wp-content/uploads/2016/11/icon-repair.png";

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CategoryItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}