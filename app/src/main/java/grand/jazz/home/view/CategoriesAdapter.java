
package grand.jazz.home.view;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import grand.jazz.R;
import grand.jazz.databinding.ItemCategoryBinding;
import grand.jazz.home.response.CategoryItem;
import grand.jazz.home.viewmodel.CategoryItemViewModel;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private List<CategoryItem> categoryItems;


    public CategoriesAdapter() {
        this.categoryItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        CategoryItem dataModel = categoryItems.get(position);
        CategoryItemViewModel categoryItemViewModel = new CategoryItemViewModel(dataModel,position);

        categoryItemViewModel.getItemsOperationsLiveListener().observeForever(new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                notifyItemChanged(position);
            }
        });
        holder.setViewModel(categoryItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.categoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<CategoryItem> data) {

            this.categoryItems.clear();

            this.categoryItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemCategoryBinding itemCategoryBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemCategoryBinding == null) {
                itemCategoryBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemCategoryBinding != null) {
                itemCategoryBinding.unbind();
            }
        }

        void setViewModel(CategoryItemViewModel categoryItemViewModel) {
            if (itemCategoryBinding != null) {
                itemCategoryBinding.setCategoryViewModel(categoryItemViewModel);
            }
        }


    }



}
