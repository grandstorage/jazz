package grand.jazz.home.view;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.gjiazhe.panoramaimageview.GyroscopeObserver;

import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.databinding.FragmentHomeBinding;
import grand.jazz.home.MyGestureListener;
import grand.jazz.home.viewmodel.HomeViewModel;

public class HomeFragment extends BaseFragment {
    private GyroscopeObserver gyroscopeObserver;
    View rootView;
    HomeViewModel homeViewModel;
    FragmentHomeBinding fragmentHomeBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentHomeBinding.getRoot();
        homeViewModel = new HomeViewModel();
        fragmentHomeBinding.setHomeViewModel(homeViewModel);
        gyroscopeObserver = new GyroscopeObserver();

        gyroscopeObserver.setMaxRotateRadian(Math.PI/9);
        fragmentHomeBinding.ivHomeHeaderPhoto.setGyroscopeObserver(gyroscopeObserver);
    }


    private void liveDataListeners() {
        homeViewModel.getClicksMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer result) {


            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        // Register GyroscopeObserver.
        gyroscopeObserver.register(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister GyroscopeObserver.
        gyroscopeObserver.unregister();
    }


}