package grand.jazz.createorder.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.jazz.base.constantsutils.WebServices;


public class CreateOrderRequest {

    @SerializedName("address")
    private String address;

    @SerializedName("date")
    private String date;

    @SerializedName("time")
    private String time;

    @SerializedName("lng")
    private String lng="23.4324";

    @SerializedName("department_id")
    private String departmentId="1";

    @SerializedName("user_id")
    private String userId="1";

    @SerializedName("cat_id")
    private String catId="4";

    @SerializedName("description")
    private String description;

    @SerializedName("type")
    private String type = WebServices.TYPE_SCHEDULED;

    @SerializedName("lat")
    private String lat="23.4324";

    @SerializedName("worker_id")
    private String workerId;

    @Expose
    String error;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLng(String  lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatId() {
        return catId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getWorkerId() {
        return workerId;
    }


    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }


    public boolean isValid() {
        return (getError() == null || getError().equals("null") )
                && getAddress() != null && !getAddress().equals("")
                && getLat() != null && !getLng().equals("0.0")
                && getType() != null && !getType().equals("")
                && getDescription() != null && !getDescription().equals("")
                && getUserId()!=null && !getUserId().equals("0")
                && (getType().equals(WebServices.TYPE_URGENT)
                || (getType().equals(WebServices.TYPE_SCHEDULED)
                && getDate() != null && !getDate().equals("") && getTime() != null
                && !getTime().equals("")));
    }

    @Override
    public String toString() {
        return
                "CreateOrderRequest{" +
                        "address = '" + address + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",department_id = '" + departmentId + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",cat_id = '" + catId + '\'' +
                        ",description = '" + description + '\'' +
                        ",type = '" + type + '\'' +
                        ",lat = '" + lat + '\'' +
                        ",worker_id = '" + workerId + '\'' +
                        "}";
    }
}