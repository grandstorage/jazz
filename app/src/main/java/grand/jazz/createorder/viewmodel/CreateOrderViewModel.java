package grand.jazz.createorder.viewmodel;


import android.view.View;
import java.util.ArrayList;
import grand.jazz.base.BaseViewModel;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.filesutils.VolleyFileObject;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.createorder.request.CreateOrderRequest;
import grand.jazz.createorder.response.CreateOrderResponse;


public class CreateOrderViewModel extends BaseViewModel {
    ArrayList<VolleyFileObject> volleyFileObjects;

    CreateOrderRequest createOrderRequest;

    public CreateOrderViewModel(CreateOrderRequest createOrderRequest) {

        this.createOrderRequest = createOrderRequest;

    }


    public CreateOrderRequest getCreateOrderRequest() {
        return createOrderRequest;
    }

    public void setCreateOrderRequest(CreateOrderRequest createOrderRequest) {
        this.createOrderRequest = createOrderRequest;
    }

    public void selectImageOrVideo() {
            getClicksMutableLiveData().setValue(Codes.SELECT_ORDER_IMAGE);
    }

    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects = volleyFileObjects!=null?volleyFileObjects:new ArrayList<>();
    }

    public void selectSchedule() {
        createOrderRequest.setType(WebServices.TYPE_SCHEDULED);
        notifyChange();
    }

    public void selectUrget() {
        createOrderRequest.setType(WebServices.TYPE_URGENT);
        notifyChange();
    }

    public void createOrder() {
        if (getCreateOrderRequest().isValid()) {
            accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    accessLoadingBar(View.GONE);
                }
            }).multiPartConnect(  URLS.CREATE_ORDER, createOrderRequest,volleyFileObjects, CreateOrderResponse.class);
        }
    }
}
