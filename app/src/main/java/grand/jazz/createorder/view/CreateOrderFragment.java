package grand.jazz.createorder.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.base.MovementManager;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.filesutils.FileOperations;
import grand.jazz.base.filesutils.VolleyFileObject;
import grand.jazz.createorder.request.CreateOrderRequest;
import grand.jazz.createorder.viewmodel.CreateOrderViewModel;
import grand.jazz.databinding.FragmentCreateOrderBinding;
import grand.jazz.databinding.FragmentOrdersBinding;

public class CreateOrderFragment extends BaseFragment {
    View rootView;
    CreateOrderViewModel createOrderViewModel;
    FragmentCreateOrderBinding fragmentCreateOrderBinding;
    int imagesCounter=0;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentCreateOrderBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_order, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentCreateOrderBinding.getRoot();
        createOrderViewModel = new CreateOrderViewModel(new CreateOrderRequest());
        fragmentCreateOrderBinding.setCreateOrderViewModel(createOrderViewModel);
    }


    private void liveDataListeners() {
        createOrderViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SELECT_ORDER_IMAGE) {
                FileOperations.pickImage(getActivity());
            } else {
                MovementManager.startActivity(getActivity(), result);
            }

        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Codes.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.IMAGE+"["+imagesCounter+++"]", requestCode);
            createOrderViewModel.getVolleyFileObjects().add(volleyFileObject);
        } else {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.VIDEO, requestCode);
            createOrderViewModel.getVolleyFileObjects().add(volleyFileObject);

        }

    }


}