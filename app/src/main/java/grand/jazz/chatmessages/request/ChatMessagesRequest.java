package grand.jazz.chatmessages.request;


import com.google.gson.annotations.SerializedName;


public class ChatMessagesRequest {

	@SerializedName("user_id")
	private int userId;

	@SerializedName("worker_id")
	private int workerId;


	public ChatMessagesRequest(int userId , int workerId){
		this.userId=userId;
		this.workerId=workerId;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setWorkerId(int workerId){
		this.workerId = workerId;
	}

	public int getWorkerId(){
		return workerId;
	}

	@Override
 	public String toString(){
		return 
			"GetMessagesResponse{" + 
			"user_id = '" + userId + '\'' + 
			",worker_id = '" + workerId + '\'' + 
			"}";
		}
}