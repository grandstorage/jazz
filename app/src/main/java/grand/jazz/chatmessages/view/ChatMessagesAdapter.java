
package grand.jazz.chatmessages.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import grand.jazz.R;
import grand.jazz.chatmessages.response.ChatMessageItem;
import grand.jazz.chatmessages.viewmodel.ChatMessageItemViewModel;
import grand.jazz.databinding.ItemChatBinding;


public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.CategoriesViewHolder> {

    private List<ChatMessageItem> orderItems;


    public ChatMessagesAdapter() {
        this.orderItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ChatMessageItem dataModel = orderItems.get(position);
        ChatMessageItemViewModel orderItemViewModel = new ChatMessageItemViewModel(dataModel,position);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> notifyItemChanged(position));
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.orderItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ChatMessageItem> data) {

            this.orderItems.clear();

            this.orderItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemChatBinding itemChatBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemChatBinding == null) {
                itemChatBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemChatBinding != null) {
                itemChatBinding.unbind();
            }
        }

        void setViewModel(ChatMessageItemViewModel chatMessageItemViewModel) {
            if (itemChatBinding != null) {
                itemChatBinding.setChatItemViewModel(chatMessageItemViewModel);
            }
        }


    }



}
