package grand.jazz.chatmessages.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.chatmessages.request.ChatMessagesRequest;
import grand.jazz.chatmessages.viewmodel.ChatMessagesViewModel;
import grand.jazz.databinding.FragmentChatMessagesBinding;

public class ChatMessagesFragment extends BaseFragment {
     View rootView;
    ChatMessagesViewModel chatMessagesViewModel;
    FragmentChatMessagesBinding fragmentChatMessagesBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChatMessagesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_messages, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentChatMessagesBinding.getRoot();
        chatMessagesViewModel = new ChatMessagesViewModel(new ChatMessagesRequest(1,1));
        fragmentChatMessagesBinding.setChatMessageViewModel(chatMessagesViewModel);
    }


    private void liveDataListeners() {
        chatMessagesViewModel.getClicksMutableLiveData().observe(this, result -> {
        });
    }




}