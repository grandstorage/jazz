package grand.jazz.chatmessages.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ChatMessagesResponse{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<ChatMessageItem> data;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<ChatMessageItem> data){
		this.data = data;
	}

	public List<ChatMessageItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ChatMessagesResponse{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}