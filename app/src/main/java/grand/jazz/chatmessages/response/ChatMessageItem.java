package grand.jazz.chatmessages.response;

import com.google.gson.annotations.SerializedName;


public class ChatMessageItem {

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("worker_image")
	private String workerImage;

	@SerializedName("body")
	private String body;

	@SerializedName("send")
	private String send;

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setWorkerImage(String workerImage){
		this.workerImage = workerImage;
	}

	public String getWorkerImage(){
		return workerImage;
	}

	public void setBody(String body){
		this.body = body;
	}

	public String getBody(){
		return body;
	}

	public void setSend(String send){
		this.send = send;
	}

	public String getSend(){
		return send;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"user_image = '" + userImage + '\'' + 
			",worker_image = '" + workerImage + '\'' + 
			",body = '" + body + '\'' + 
			",send = '" + send + '\'' + 
			"}";
		}
}