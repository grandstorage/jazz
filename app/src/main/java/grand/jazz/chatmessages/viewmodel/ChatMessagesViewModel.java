package grand.jazz.chatmessages.viewmodel;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.chatmessages.request.ChatMessagesRequest;
import grand.jazz.chatmessages.request.SendMessagesRequest;
import grand.jazz.chatmessages.response.ChatMessageItem;
import grand.jazz.chatmessages.response.ChatMessagesResponse;
import grand.jazz.chatmessages.response.SendMessageResponse;
import grand.jazz.chatmessages.view.ChatMessagesAdapter;

public class ChatMessagesViewModel extends BaseViewModel {

    ChatMessagesAdapter chatMessagesAdapter;
    private ChatMessagesRequest chatMessagesRequest;
    String message;
    public ChatMessagesViewModel(ChatMessagesRequest chatMessagesRequest) {
        this.chatMessagesRequest = chatMessagesRequest;
        getServices();
    }


    @BindingAdapter({"app:adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, ChatMessagesAdapter chatMessagesAdapter) {
        recyclerView.setAdapter(chatMessagesAdapter);
    }


    @Bindable
    public ChatMessagesAdapter getChatMessagesAdapter() {
        return this.chatMessagesAdapter == null ? this.chatMessagesAdapter = new ChatMessagesAdapter() : this.chatMessagesAdapter;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    private void getServices() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ChatMessagesResponse chatMessagesResponse = (ChatMessagesResponse) response;
                getChatMessagesAdapter().updateData(chatMessagesResponse.getData());
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CHAT_MESSAGES, chatMessagesRequest, ChatMessagesResponse.class);
    }

    public void sendMessage() {
        accessLoadingBar(View.VISIBLE);
        SendMessagesRequest sendMessagesRequest = new SendMessagesRequest();
        sendMessagesRequest.setSend(WebServices.TYPE_USER);
        sendMessagesRequest.setBody(message);
        sendMessagesRequest.setUserId(chatMessagesRequest.getUserId());
        sendMessagesRequest.setWorkerId(chatMessagesRequest.getWorkerId());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.SEND_MESSAGE, sendMessagesRequest, SendMessageResponse.class);
    }
}
