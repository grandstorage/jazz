package grand.jazz.register.view;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.base.MovementManager;
import grand.jazz.base.PopUpItem;
import grand.jazz.base.PopUpMenus;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.filesutils.FileOperations;
import grand.jazz.base.filesutils.VolleyFileObject;
import grand.jazz.databinding.FragmentRegisterBinding;
import grand.jazz.register.viewmodel.RegisterViewModel;

public class RegisterFragment extends BaseFragment {

    View rootView;
    RegisterViewModel registerViewModel;
    FragmentRegisterBinding fragmentRegisterBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentRegisterBinding.getRoot();
        registerViewModel = new RegisterViewModel();
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);
    }


    private void liveDataListeners() {
        registerViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if (result == Codes.SELECT_PROFILE_IMAGE) {
                FileOperations.pickImage(getActivity());
            } else if (result == Codes.SELECT_COMMERICAL_IMAGE) {
                FileOperations.pickFile(getActivity());
            } else if (result == Codes.SELECT_ACCOUNT_TYPE) {
               showUserTypes();
            } else if (result == Codes.SHOW_MESSAGE) {
                showMessage(registerViewModel.getReturnedMessage());
            }else{
                MovementManager.startActivity(getActivity(), result);
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Codes.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.IMAGE, requestCode);
            fragmentRegisterBinding.profileImage.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
            registerViewModel.getVolleyFileObjects().add(volleyFileObject);
        } else {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.COMMERICAL_REGISTER, requestCode);
            registerViewModel.setFilePath(volleyFileObject.getFilePath());
            registerViewModel.getVolleyFileObjects().add(FileOperations.getVolleyFileObject(getActivity(), data, WebServices.COMMERICAL_REGISTER, requestCode));
            registerViewModel.notifyChange();
        }
    }

    private void showUserTypes(){
        final ArrayList<PopUpItem> popUpItems = PopUpMenus.getUserTypes(getActivity());
        PopUpMenus.showPopUp(getActivity(), fragmentRegisterBinding.etRegisterUserTypes, popUpItems).setOnMenuItemClickListener(item -> {
            registerViewModel.setUserType(popUpItems.get(item.getItemId()).getName());
            registerViewModel.getRegisterRequest().setRole(popUpItems.get(item.getItemId()).getId());
            registerViewModel.notifyChange();
            return false;
        });
    }
}