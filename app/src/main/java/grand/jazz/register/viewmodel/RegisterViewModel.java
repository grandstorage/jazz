package grand.jazz.register.viewmodel;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import grand.jazz.base.BaseViewModel;
import grand.jazz.base.UserPreferenceHelper;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.filesutils.VolleyFileObject;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.MyApplication;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.register.request.RegisterRequest;
import grand.jazz.register.response.RegisterResponse;

public class RegisterViewModel extends BaseViewModel {
    private  RegisterRequest registerRequest;
    private String filePath;
    private String userType;
    private ArrayList<VolleyFileObject>volleyFileObjects;



    public RegisterViewModel() {
        registerRequest = new RegisterRequest();
        volleyFileObjects = new ArrayList<>();
    }


    public void accountTypeClick(){
        getClicksMutableLiveData().setValue(Codes.SELECT_ACCOUNT_TYPE);
    }

    public void uploadPhotoClick(){
        getClicksMutableLiveData().setValue(Codes.SELECT_PROFILE_IMAGE);
    }

    public void uploadCommericalClick(){
        getClicksMutableLiveData().setValue(Codes.SELECT_COMMERICAL_IMAGE);
    }

    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public String getFilePath() {
        return filePath;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setRegisterRequest(RegisterRequest registerRequest) {
        this.registerRequest = registerRequest;
    }

    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }


    public void registerClick() {
        notifyChange();
        goRegister();
    }

    private void goRegister() {

        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                RegisterResponse registerResponse = (RegisterResponse)response;
                if(registerResponse.getStatus().equals(WebServices.SUCCESS)){
                     getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);

                }else  if(registerResponse.getStatus().equals(WebServices.FAILED)){
                    setReturnedMessage(registerResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect( URLS.REGISTER, registerRequest,volleyFileObjects, RegisterResponse.class);
    }
}
