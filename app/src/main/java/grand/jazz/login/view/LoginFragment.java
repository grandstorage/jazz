package grand.jazz.login.view;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.base.MovementManager;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.databinding.FragmentLoginBinding;
import grand.jazz.login.viewmodel.LoginViewModel;

public class LoginFragment extends BaseFragment {

    View rootView;

    LoginViewModel loginViewModel;
    FragmentLoginBinding fragmentLoginBinding;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentLoginBinding.getRoot();
        loginViewModel = new LoginViewModel();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
    }


    private void liveDataListeners() {
        loginViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SEND_CODE_SCREEN || result == Codes.REGISTER_SCREEN || result == Codes.FORGOT_PASSWORD_SCREEN) {
                MovementManager.startActivity(getActivity(), result);
            } else if (result == Codes.HOME_SCREEN) {
                MovementManager.startMainActivity(getActivity(), result);
            } else if (result == Codes.SHOW_MESSAGE) {
                    showMessage(getActivity().getString(R.string.msg_wrong_password));
            }
        });
    }


}