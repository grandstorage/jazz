package grand.jazz.login.viewmodel;

import android.databinding.Bindable;
import android.util.Log;
import android.view.View;
import com.android.volley.Request;

import grand.jazz.base.UserPreferenceHelper;
import grand.jazz.base.UserRequest;
import grand.jazz.base.BaseViewModel;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.MyApplication;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.login.response.UserItem;
import grand.jazz.login.response.UserResponse;


public class LoginViewModel extends BaseViewModel {
    UserRequest userRequest;


    public LoginViewModel() {

        userRequest = new UserRequest();
    }

    @Bindable
    public UserRequest getUserDetails() {
        return userRequest;
    }


    public void loginClick() {

        goLogin();
    }

    public void newAccountClick() {
        getClicksMutableLiveData().setValue(Codes.REGISTER_SCREEN);
    }


    public void forgotPassword() {
        getClicksMutableLiveData().setValue(Codes.FORGOT_PASSWORD_SCREEN);
    }

    private void goLogin() {

        if (getUserDetails().isValid()) {
            accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    accessLoadingBar(View.GONE);
                    UserResponse userResponse = (UserResponse) response;
                    if(userResponse.getStatus().equals(WebServices.SUCCESS)){
                       UserItem userItem = userResponse.getData();
                        userItem.setPassword(userRequest.getPassword());
                        UserPreferenceHelper.saveUserDetails(MyApplication.getInstance().getApplicationContext(),userItem);
                        getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    }
                    else if(userResponse.getStatus().equals(WebServices.NOT_ACTIVE)){
                        getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                    }else if(userResponse.getStatus().equals(WebServices.FAILED)){
                        getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                    }
                }
            }).requestJsonObject(Request.Method.POST, URLS.LOGIN, userRequest, UserResponse.class);
        }
    }

}
