package grand.jazz.login.response;


import com.google.gson.annotations.SerializedName;

 public class UserResponse{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private UserItem data;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(UserItem data){
		this.data = data;
	}

	public UserItem getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UserResponse{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}