package grand.jazz.sendcode.viewmodel;

import android.view.View;

import com.android.volley.Request;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.sendcode.request.SendCodeRequest;
import grand.jazz.sendcode.response.SendCodeResponse;

public class SendCodeViewModel extends BaseViewModel {
    SendCodeRequest sendCodeRequest;


    public SendCodeViewModel() {
        sendCodeRequest = new SendCodeRequest();
    }


    public void sendCodeClick() {
        sendCode();
    }


    public SendCodeRequest getSendCodeRequest() {
        return sendCodeRequest;
    }


    private void sendCode() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }
}
