package grand.jazz.sendcode.view;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.base.MovementManager;
import grand.jazz.base.PopUpItem;
import grand.jazz.base.PopUpMenus;
import grand.jazz.base.constantsutils.Codes;
import grand.jazz.base.constantsutils.WebServices;
import grand.jazz.base.filesutils.FileOperations;
import grand.jazz.base.filesutils.VolleyFileObject;
import grand.jazz.databinding.FragmentRegisterBinding;
import grand.jazz.databinding.FragmentSendCodeBinding;
import grand.jazz.sendcode.viewmodel.SendCodeViewModel;

public class SendCodeFragment extends BaseFragment {

    View rootView;
    SendCodeViewModel sendCodeViewModel;
    FragmentSendCodeBinding fragmentSendCodeBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSendCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_code, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSendCodeBinding.getRoot();
        sendCodeViewModel = new SendCodeViewModel();
        fragmentSendCodeBinding.setSendCodeViewModel(sendCodeViewModel);
    }


    private void liveDataListeners() {
        sendCodeViewModel.getClicksMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer result) {

                if (result == View.VISIBLE || result == View.GONE) {
                    accessLoadingBar(result);
                } else if (result == Codes.HOME_SCREEN) {
                    MovementManager.startActivity(getActivity(),result);
                }else if(result == Codes.SHOW_MESSAGE){
                    showMessage(sendCodeViewModel.getReturnedMessage());

                }

            }
        });
    }


}