package grand.jazz.sendcode.request;


import com.google.gson.annotations.SerializedName;

import grand.jazz.base.BaseModel;

public class SendCodeRequest  {
	String error;
	 String code;

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}


}