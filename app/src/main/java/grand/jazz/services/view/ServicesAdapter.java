
package grand.jazz.services.view;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import grand.jazz.R;
import grand.jazz.databinding.ItemServiceBinding;
import grand.jazz.services.response.ServiceItem;
import grand.jazz.services.viewmodel.ServiceItemViewModel;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.CategoriesViewHolder> {

    private List<ServiceItem> serviceItems;


    public ServicesAdapter() {
        this.serviceItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        ServiceItem dataModel = serviceItems.get(position);
        ServiceItemViewModel categoryItemViewModel = new ServiceItemViewModel(dataModel,position);

        categoryItemViewModel.getItemsOperationsLiveListener().observeForever(new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                notifyItemChanged(position);
            }
        });
        holder.setViewModel(categoryItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.serviceItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ServiceItem> data) {

            this.serviceItems.clear();

            this.serviceItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemServiceBinding   itemServiceBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemServiceBinding == null) {
                itemServiceBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemServiceBinding != null) {
                itemServiceBinding.unbind();
            }
        }

        void setViewModel(ServiceItemViewModel serviceItemViewModel) {
            if (itemServiceBinding != null) {
                itemServiceBinding.setServiceItemViewModel(serviceItemViewModel);
            }
        }


    }



}
