package grand.jazz.services.view;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.gjiazhe.panoramaimageview.GyroscopeObserver;
import grand.jazz.R;
import grand.jazz.base.BaseFragment;
import grand.jazz.databinding.FragmentServicesBinding;
import grand.jazz.services.request.ServiceRequest;
import grand.jazz.services.viewmodel.ServicesViewModel;

public class ServicesFragment extends BaseFragment {
    private GyroscopeObserver gyroscopeObserver;
    View rootView;
    ServicesViewModel servicesViewModel;
    FragmentServicesBinding fragmentServicesBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentServicesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_services, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentServicesBinding.getRoot();
        servicesViewModel = new ServicesViewModel(new ServiceRequest(1));
        fragmentServicesBinding.setServiceViewModel(servicesViewModel);
        gyroscopeObserver = new GyroscopeObserver();
        gyroscopeObserver.setMaxRotateRadian(Math.PI/9);
        fragmentServicesBinding.ivHomeHeaderPhoto.setGyroscopeObserver(gyroscopeObserver);
    }


    private void liveDataListeners() {
        servicesViewModel.getClicksMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer result) {


            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        gyroscopeObserver.register(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        gyroscopeObserver.unregister();
    }


}