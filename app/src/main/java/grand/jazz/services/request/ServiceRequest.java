package grand.jazz.services.request;

import com.google.gson.annotations.SerializedName;

public class ServiceRequest {

    @SerializedName("id")
    int id;

    public ServiceRequest(int id){
        this.id=id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
