package grand.jazz.services.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ServicesResponse{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<ServiceItem> data;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<ServiceItem> data){
		this.data = data;
	}

	public List<ServiceItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ServicesResponse{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}