package grand.jazz.services.viewmodel;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;

import grand.jazz.base.BaseViewModel;
import grand.jazz.base.volleyutils.ConnectionHelper;
import grand.jazz.base.volleyutils.ConnectionListener;
import grand.jazz.base.volleyutils.URLS;
import grand.jazz.services.request.ServiceRequest;
import grand.jazz.services.response.ServicesResponse;
import grand.jazz.services.view.ServicesAdapter;

public class ServicesViewModel extends BaseViewModel {

    ServicesAdapter servicesAdapter;

    public ServicesViewModel(ServiceRequest serviceRequest) {
        getServices(serviceRequest);
    }


    @BindingAdapter({"app:adapter"})
    public static void getServicesBinding(RecyclerView recyclerView, ServicesAdapter categoriesAdapter) {
        recyclerView.setAdapter(categoriesAdapter);
    }


    @Bindable
    public ServicesAdapter getServicesAdapter() {
        return this.servicesAdapter == null ? this.servicesAdapter = new ServicesAdapter() : this.servicesAdapter;
    }


    private void getServices(ServiceRequest serviceRequest) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ServicesResponse servicesResponse = (ServicesResponse) response;
                getServicesAdapter().updateData(servicesResponse.getData());
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, URLS.SERVICES, serviceRequest, ServicesResponse.class);
    }
}
